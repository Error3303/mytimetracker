package com.error3303.mytimetracker.repositories;

import androidx.lifecycle.LiveData;

import com.error3303.mytimetracker.services.ServiceTime;

import java.util.Date;

public class ActivityRepository {

    ServiceTime serviceTime;

    public ActivityRepository(){
        serviceTime = new ServiceTime();
    }

    public LiveData<Date> getCurrentTime(){
        return serviceTime.getCurrentTime();
    }
}
