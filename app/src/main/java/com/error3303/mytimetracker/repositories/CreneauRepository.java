package com.error3303.mytimetracker.repositories;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.error3303.mytimetracker.database.ServicePersistance;
import com.error3303.mytimetracker.models.Creneau;

import java.util.List;

public class CreneauRepository {

    ServicePersistance servicePersistance;
    LiveData<List<Creneau>> allCreneaux;

    /**
     * Constructeur
     * @param application
     */
    public CreneauRepository(Application application){
        servicePersistance = new ServicePersistance(application);
        allCreneaux = servicePersistance.getCreneaux();
    }

    /**
     * Insère un creneau en bdd.
     * @param creneau le creneau à insérer.
     */
    public void insert(Creneau creneau){
        new InsertCreneauASyncTask(servicePersistance).doInBackground(creneau);
    }

    /**
     * Mets à jour un creneau présent en bdd.
     * @param creneau le creneau à mettre à jour.
     */
    public void update(Creneau creneau){
        new UpdateCreneauASyncTask(servicePersistance).doInBackground(creneau);
    }

    /**
     * Supprime un creneau en bdd.
     * @param creneau le creneau à supprimer.
     */
    public void delete(Creneau creneau){
        new DeleteCreneauASyncTask(servicePersistance).doInBackground(creneau);
    }

    /**
     * Retourne la liste des créneaux présent en bdd.
     * @Return un LiveData contenant la liste des creneaux.
     */
    public LiveData<List<Creneau>> getAllCreneaux(){
        return allCreneaux;
    }

    /**
     * Thread pour l'insertion d'un créneau.
     */
    private static class InsertCreneauASyncTask extends AsyncTask<Creneau, Void, Void>{
        ServicePersistance servicePersistance;

        public InsertCreneauASyncTask(ServicePersistance servicePersistance){
            this.servicePersistance = servicePersistance;
        }

        @Override
        protected Void doInBackground(Creneau... creneaux) {
            servicePersistance.insert(creneaux[0]);
            return null;
        }
    }

    /**
     * Thread pour la mise à jour d'un créneau.
     */
    private static class UpdateCreneauASyncTask extends AsyncTask<Creneau, Void, Void>{
        ServicePersistance servicePersistance;

        public UpdateCreneauASyncTask(ServicePersistance servicePersistance){
            this.servicePersistance = servicePersistance;
        }

        @Override
        protected Void doInBackground(Creneau... creneaux) {
            servicePersistance.update(creneaux[0]);
            return null;
        }
    }

    /**
     * Thread pour la suppression d'un créneau.
     */
    private static class DeleteCreneauASyncTask extends AsyncTask<Creneau, Void, Void>{
        ServicePersistance servicePersistance;

        public DeleteCreneauASyncTask(ServicePersistance servicePersistance){
            this.servicePersistance = servicePersistance;
        }

        @Override
        protected Void doInBackground(Creneau... creneaux) {
            servicePersistance.delete(creneaux[0]);
            return null;
        }
    }
}
