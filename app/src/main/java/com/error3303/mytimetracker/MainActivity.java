package com.error3303.mytimetracker;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.error3303.mytimetracker.dialogfragment.DialogFragmentCreneau;
import com.error3303.mytimetracker.models.Creneau;
import com.error3303.mytimetracker.util.DateUtil;
import com.error3303.mytimetracker.viewmodel.ActivityViewModel;
import com.error3303.mytimetracker.viewmodel.CreneauViewModel;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements DialogFragmentCreneau.DialogFragmentCreneauListener {

    @BindView(R.id.lbl_date)
    TextView lblDate;
    @BindView(R.id.lbl_time)
    TextView lblTime;
    @BindView(R.id.btn_start_stop)
    Button btnStartStop;
    @BindView(R.id.btn_historique)
    Button btnHistorique;
    @BindView(R.id.btn_settings)
    Button btnSettings;

    private boolean start = false;

    private ActivityViewModel activityViewModel;
    private CreneauViewModel creneauViewModel;
    private Creneau currentCreneau;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        init();
    }

    /**
     * Initialise les données de l'activity.
     */
    private void init() {
        activityViewModel = ViewModelProviders.of(this).get(ActivityViewModel.class);
        creneauViewModel = ViewModelProviders.of(this).get(CreneauViewModel.class);
        activityViewModel.getCurrentTime().observe(this, currentTime -> updateTime(currentTime));
    }

    /**
     * Actualises la date et l'heure à chaque seconde du systeme.
     */
    private void updateTime(Date currentTime) {
        runOnUiThread(() -> {
            lblDate.setText(DateUtil.dateToStringFormatDate(currentTime));
            lblTime.setText(DateUtil.dateToStringFormatTime(currentTime));
        });

        if (start && activatedNotif) {
            if (!notifPushed) {
                if (timerNotif < (minuteDelayNotif * 60)) {
                    timerNotif++;
                } else {
                    sendNewNotification("Alerte dépassement timer",
                            String.format("Cela fait maintenant %d minutes que vous avez lancé le timer !", minuteDelayNotif));
                    notifPushed = true;
                }
            }
        }
    }



    }

    /**
     * Permet le démarrage ou l'arrêt d'un enregistrement de creneau.
     *      Si pas d'enregistrement en cours -> Démarrage de l'enregistrement
     *      Sinon -> Arret de l'enregistrement et ouverture d'une boite
     *      de dialogue pour saisir le libelle de l'enregistrement.
     */
    @OnClick(R.id.btn_start_stop)
    public void startStop(){
        start = !start;

        if(start){
            currentCreneau = new Creneau();
            btnStartStop.setText("Arreter");
        }else{
            currentCreneau.setStopTime(new Date().getTime());
            currentCreneau.setLibelle("Ouvrir dialog");

            DialogFragmentCreneau dialogCreneau = new DialogFragmentCreneau();
            dialogCreneau.show(getSupportFragmentManager(), "creneau dialog");

            btnStartStop.setText("Démarrer");
        }
    }



    /**
     * Permet de récupérer la saisie effectuée dans le dialogFragment.
     * @param libelle chaine de caractères saisie par l'utilisateur.
     */
    @Override
    public void apply(String libelle) {
        currentCreneau.setLibelle(libelle);
        creneauViewModel.insert(currentCreneau);
    }
}