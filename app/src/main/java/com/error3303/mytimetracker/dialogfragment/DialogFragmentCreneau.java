package com.error3303.mytimetracker.dialogfragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.error3303.mytimetracker.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DialogFragmentCreneau extends AppCompatDialogFragment {

    @BindView(R.id.edit_libelle_creneau)
    EditText editLibelleCreneau;
    @BindView(R.id.btn_save_libelle_creneau)
    Button btnSaveLibelleCreneau;

    private DialogFragmentCreneauListener listener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_dialog_creneau, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        try {
            listener = (DialogFragmentCreneauListener) getContext();
        } catch (ClassCastException e) {
            Log.e(getContext().toString(), "la classe doit implémenter DialogFragmentCreneauListener");
        }
    }

    @OnClick(R.id.btn_save_libelle_creneau)
    public void saveEditLibelle(){
        this.listener.apply(editLibelleCreneau.getText().toString());
        this.dismiss();
    }

    /**
     * Listener des données saisies dans le fragment.
     */
    public interface DialogFragmentCreneauListener{
        void apply(String libelle);
    }
}
