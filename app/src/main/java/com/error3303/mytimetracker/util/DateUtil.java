package com.error3303.mytimetracker.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    private static final String DATE_PATTERN = "EEEE dd MMM yyyy";
    private static final String TIME_PATTERN = "HH:mm:ss";

    /**
     * Formate une date selon le format "Jour XX Mois XXXX".
     * @param date la date à formater.
     * @return une chaine de caractères au format EEEE dd MMM yyyy.
     */
    public static String dateToStringFormatDate(Date date) {
        SimpleDateFormat sdfDate = new SimpleDateFormat(DATE_PATTERN, Locale.FRANCE);
        return sdfDate.format(date);
    }

    /**
     * Formate une date en heure.
     * @param date la date à formater.
     * @return une chaine de caractères au format HH:mm:ss.
     */
    public static String dateToStringFormatTime(Date date){
        SimpleDateFormat sdfTime = new SimpleDateFormat(TIME_PATTERN, Locale.FRANCE);
        return sdfTime.format(date);
    }
}
