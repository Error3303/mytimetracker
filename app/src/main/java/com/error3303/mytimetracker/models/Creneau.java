package com.error3303.mytimetracker.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Creneau {

    @PrimaryKey(autoGenerate = true)
    int id;

    long startTime;

    long stopTime;

    String libelle;

    public Creneau(){
        startTime = new Date().getTime();
    }
}
