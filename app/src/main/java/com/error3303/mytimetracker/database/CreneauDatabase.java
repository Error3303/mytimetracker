package com.error3303.mytimetracker.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.error3303.mytimetracker.models.Creneau;

@Database(entities = {Creneau.class}, version = 1, exportSchema = false)
public abstract class CreneauDatabase extends RoomDatabase {

    // --- DAO ---
    public abstract CreneauDao creneauDao();

    /**
     * Retourne  une instance de CreneauDatabase
     * @param context
     * @return CreneauDatabase
     */
    public static CreneauDatabase getInstance(Context context) {
        synchronized (Creneau.class) {
            return Room.databaseBuilder(context,
                    CreneauDatabase.class, "database.db")
                    .allowMainThreadQueries()
                    .build();
        }
    }

}
