package com.error3303.mytimetracker.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.error3303.mytimetracker.models.Creneau;

import java.util.List;

@Dao
public interface CreneauDao {

    @Query("SELECT * FROM Creneau")
    LiveData<List<Creneau>> getCreneaux();

    @Insert
    void insertCreneau(Creneau creneau);

    @Update
    void updateCreneau(Creneau creneau);

    @Delete
    void deleteCreneau(Creneau creneau);
}
