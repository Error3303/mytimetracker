package com.error3303.mytimetracker.database;

import androidx.lifecycle.LiveData;

import com.error3303.mytimetracker.models.Creneau;

import java.util.List;

public interface IServicePersistance {

    LiveData<List<Creneau>> getCreneaux();

    void insert(Creneau creneau);

    void update(Creneau creneau);

    void delete(Creneau creneau);
}
