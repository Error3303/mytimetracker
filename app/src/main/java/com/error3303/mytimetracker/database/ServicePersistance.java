package com.error3303.mytimetracker.database;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.error3303.mytimetracker.MyApplication;
import com.error3303.mytimetracker.models.Creneau;

import java.util.List;

public class ServicePersistance implements IServicePersistance {

    private CreneauDao dao;

    /**
     * Consructeur
     * @param application
     */
    public ServicePersistance(Application application){
        dao = ((MyApplication) application).getDao();
    }

    /**
     * Retourne la liste des creneaux en bdd.
     * @return List<Creneau>
     */
    @Override
    public LiveData<List<Creneau>> getCreneaux() {
        return dao.getCreneaux();
    }

    /**
     * Insere un creneau en bdd.
     * @param creneau le creneau à ajouter.
     */
    @Override
    public void insert(Creneau creneau) {
        dao.insertCreneau(creneau);
    }

    /**
     * Met à jour un creneau en bdd.
     * @param creneau le creneau à mettre à jour.
     */
    @Override
    public void update(Creneau creneau) {
        dao.updateCreneau(creneau);
    }

    /**
     * Supprime un creneau de la bdd.
     * @param creneau le creneau à supprimer.
     */
    @Override
    public void delete(Creneau creneau) {
        dao.deleteCreneau(creneau);
    }
}
