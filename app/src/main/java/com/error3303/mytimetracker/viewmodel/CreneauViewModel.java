package com.error3303.mytimetracker.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.error3303.mytimetracker.models.Creneau;
import com.error3303.mytimetracker.repositories.CreneauRepository;

import java.util.List;

public class CreneauViewModel extends AndroidViewModel {

    private CreneauRepository repository;
    private LiveData<List<Creneau>> allCreneaux;

    /**
     * Constructeur
     * @param application
     */
    public CreneauViewModel(@NonNull Application application) {
        super(application);
        repository = new CreneauRepository(application);
        allCreneaux = repository.getAllCreneaux();
    }

    /**
     * Insère un creneau en bdd.
     * @param creneau creneau à insérer.
     */
    public void insert(Creneau creneau){
        repository.insert(creneau);
    }

    /**
     * Mets à jour un creneau en bdd.
     * @param creneau creneau à mettre à jour.
     */
    public void update(Creneau creneau){
        repository.update(creneau);
    }

    /**
     * Supprime un creneau de la bdd.
     * @param creneau creneau à supprimer.
     */
    public void delete(Creneau creneau){
        repository.delete(creneau);
    }

    /**
     * Retourne la liste des creneaux de la bdd.
     * @return un LiveData contenant la liste des créneaux.
     */
    public LiveData<List<Creneau>> getAllCreneaux(){
        return allCreneaux;
    }
}
