package com.error3303.mytimetracker.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.error3303.mytimetracker.repositories.ActivityRepository;

import java.util.Date;

public class ActivityViewModel extends AndroidViewModel {

    ActivityRepository repository;

    public ActivityViewModel(@NonNull Application application) {
        super(application);
        repository = new ActivityRepository();
    }

    public LiveData<Date> getCurrentTime(){
        return repository.getCurrentTime();
    }
}
