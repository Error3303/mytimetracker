package com.error3303.mytimetracker.services;

import androidx.lifecycle.LiveData;

import java.util.Date;

public interface IServiceTime {

    LiveData<Date> getCurrentTime();

}
