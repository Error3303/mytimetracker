package com.error3303.mytimetracker.services;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class ServiceTime implements IServiceTime {

    private MutableLiveData<Date> currentTime;

    public ServiceTime() {
        currentTime = new MutableLiveData<>();
        startTimer();
    }

    @Override
    public LiveData<Date> getCurrentTime() {
        return currentTime;
    }

    private void startTimer(){
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                Calendar cal = Calendar.getInstance();
                currentTime.postValue(cal.getTime());
            }
        }, 0, 1000);
    }
}
