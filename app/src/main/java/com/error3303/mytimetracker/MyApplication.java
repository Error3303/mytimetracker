package com.error3303.mytimetracker;

import android.app.Application;

import com.error3303.mytimetracker.database.CreneauDao;
import com.error3303.mytimetracker.database.CreneauDatabase;

import lombok.Getter;

public class MyApplication extends Application {

    @Getter
    private volatile CreneauDao dao;

    private CreneauDatabase database;

    /**
     * Permet la récupération de la dao au lancement de l'application.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        database = CreneauDatabase.getInstance(getApplicationContext());
        dao = database.creneauDao();
    }

    /**
     * Permet la fermeture de la base de données
     */
    public void closeDatabase() {
        if (database != null) {
            database.getOpenHelper().close();
        }
    }
}
